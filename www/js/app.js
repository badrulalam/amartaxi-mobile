// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'uiGmapgoogle-maps', 'starter.controllers', 'starter.services'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })


    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

        // Turn off caching for demo simplicity's sake
        $ionicConfigProvider.views.maxCache(0);

        /*
         // Turn off back button text
         $ionicConfigProvider.backButton.previousTitleText(false);
         */

        $stateProvider
            .state('select', {
                url: '/select',
                templateUrl: 'templates/select.html',
                controller: 'SelectCtrl'
            })
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.activity', {
                url: '/activity',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/activity.html',
                        controller: 'ActivityCtrl'
                    },
                    'fabContent': {
                        template: '<button id="fab-activity" class="button button-fab button-fab-top-right expanded button-energized-900 flap"><i class="icon ion-paper-airplane"></i></button>',
                        controller: function ($timeout) {
                            $timeout(function () {
                                document.getElementById('fab-activity').classList.toggle('on');
                            }, 200);
                        }
                    }
                }
            })

            .state('app.friends', {
                url: '/friends',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/friends.html',
                        controller: 'FriendsCtrl'
                    },
                    'fabContent': {
                        template: '<button id="fab-friends" class="button button-fab button-fab-top-left expanded button-energized-900 spin"><i class="icon ion-chatbubbles"></i></button>',
                        controller: function ($timeout) {
                            $timeout(function () {
                                document.getElementById('fab-friends').classList.toggle('on');
                            }, 900);
                        }
                    }
                }
            })

            .state('app.gallery', {
                url: '/gallery',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/gallery.html',
                        controller: 'GalleryCtrl'
                    },
                    'fabContent': {
                        template: '<button id="fab-gallery" class="button button-fab button-fab-top-right expanded button-energized-900 drop"><i class="icon ion-heart"></i></button>',
                        controller: function ($timeout) {
                            $timeout(function () {
                                document.getElementById('fab-gallery').classList.toggle('on');
                            }, 600);
                        }
                    }
                }
            })

            .state('app.driver_login', {
                url: '/driver-login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/driver_login.html',
                        controller: 'DriverLoginCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.driver_login_form', {
                url: '/driver-login-form',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/driver_login_form.html',
                        controller: 'DriverLoginFormCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.driver_register_form', {
                url: '/driver-register-form',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/driver_register_form.html',
                        controller: 'DriverRegisterFormCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })

            .state('app.passenger_login', {
                url: '/passenger-login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/passenger_login.html',
                        controller: 'PassengerLoginCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.passenger_login_form', {
                url: '/passenger-login-form',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/passenger_login_form.html',
                        controller: 'PassengerLoginFormCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.passenger_register_form', {
                url: '/passenger_register_form',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/passenger_register_form.html',
                        controller: 'PassengerRegisterFormCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.profile', {
                url: '/profile',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/profile.html',
                        controller: 'ProfileCtrl'
                    },
                    'fabContent': {
                        template: '<button id="fab-profile" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-plus"></i></button>',
                        controller: function ($timeout) {
                            /*$timeout(function () {
                             document.getElementById('fab-profile').classList.toggle('on');
                             }, 800);*/
                        }
                    }
                }
            })
            .state('app.driver_home', {
                url: '/driver-home',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/driver_home.html',
                        controller: 'DriverHomeCtrl'
                    },
                    'fabContent': {
                        template: ''
                        // template: '<button id="fab-profile" class="button button-fab button-fab-bottom-right button-energized-900"><i class="icon ion-plus"></i></button>',
                        // controller: function ($timeout) {
                        //     /*$timeout(function () {
                        //         document.getElementById('fab-profile').classList.toggle('on');
                        //     }, 800);*/
                        // }
                    }
                }
            })

            .state('app.passenger_home', {
                url: '/passenger-home',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/passenger_home.html',
                        controller: 'PassengerHomeCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.choose_pickup_point', {
                url: '/choose-pickup-point',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/passenger/choose_pickup_point.html',
                        controller: 'ChoosePickupPointCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.nearby_locations', {
                url: '/nearby-locations',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/passenger/nearby_locations.html',
                        controller: 'NearbyLocationsCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.select_on_map', {
                url: '/select-on-map',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/passenger/select_on_map.html',
                        controller: 'SelectOnMapCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.pickup_up_selected', {
                url: '/pickup-up-selected',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/passenger/select_on_map.html',
                        controller: 'PickupUpSelectedCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.select_destination', {
                url: '/select-destination',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/passenger/select_destination.html',
                        controller: 'SelectDestinationCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.select_destination_on_map', {
                url: '/select-destination-on-map',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/passenger/select_destination_on_map.html',
                        controller: 'SelectDestinationOnMapCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            .state('app.itinerary_selected', {
                url: '/itinerary-selected',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/passenger/itinerary_selected.html',
                        controller: 'ItinerarySelectedCtrl'
                    },
                    'fabContent': {
                        template: ''
                    }
                }
            })
            ;


        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/passenger-login-form');
    });
