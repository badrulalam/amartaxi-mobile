angular.module('starter.services', [])
    .constant('AccessLevels', {
        anon: 0,
        user: 1
    })
    .constant('Constants', {
        BaseURL: 'http://localhost:1337'
    })
    .factory('LocalService', function () {
        return {
            get: function (key) {
                return localStorage.getItem(key);
            },
            set: function (key, val) {
                return localStorage.setItem(key, val);
            },
            unset: function (key) {
                return localStorage.removeItem(key);
            }
        }
    })
    .factory('Auth', function ($http, LocalService, AccessLevels, Constants) {
        return {
            authorize: function (access) {
                if (access === AccessLevels.user) {
                    return this.isAuthenticated();
                } else {
                    return true;
                }
            },
            isAuthenticated: function () {
                return LocalService.get('auth_token');
            },
            login: function (credentials) {
                //104.236.249.183
                console.log("inside login auth: " + JSON.stringify(credentials).toString());
                var login = $http.post(Constants.BaseURL + '/auth/authenticate', credentials).
                    success(function (result) {
                        console.log("auth success");

                        LocalService.set('auth_token', JSON.stringify(result));
                    }).error(function (result) {
                        console.log("login error: " + result);
                    });
                //console.log("webroot: " + $rootScope.webRoot);
                return login;
            },
            logout: function () {
                // The backend doesn't care about logouts, delete the token and you're good to go.
                LocalService.unset('auth_token');
            },
            register: function (formData) {
                LocalService.unset('auth_token');
                var register = $http.post(Constants.BaseURL + '/auth/register', formData);
                register.success(function (result) {
                    LocalService.set('auth_token', JSON.stringify(result));
                });
                return register;
            }
        }
    })
    .factory('CarAuth', function ($http, LocalService, AccessLevels, Constants, $rootScope) {
        return {
            authorize: function (access) {
                if (access === AccessLevels.user) {
                    return this.isAuthenticated();
                } else {
                    return true;
                }
            },
            isAuthenticated: function () {
                return LocalService.get('auth_token');
            },
            login: function (credentials) {
              console.log(credentials);
                //104.236.249.183
                console.log("inside login auth: " + JSON.stringify(credentials).toString());
                var login = $http.post(Constants.BaseURL + '/carauth/authenticate', credentials).
                    success(function (result) {
                        console.log("auth success");

                        LocalService.set('auth_token', JSON.stringify(result));
                        $rootScope.CurrentDriver = result;
                    }).error(function (result) {
                        console.log( result);
                    });
                //console.log("webroot: " + $rootScope.webRoot);
                return login;
            },
            logout: function () {
                // The backend doesn't care about logouts, delete the token and you're good to go.
                LocalService.unset('auth_token');
            },
            register: function (formData) {
                console.log(formData);
                LocalService.unset('auth_token');
                var register = $http.post(Constants.BaseURL + '/carauth/register', formData);
                register.success(function (result) {
                    LocalService.set('auth_token', JSON.stringify(result));
                    $rootScope.CurrentDriver = result;
                });
                return register;
              }
        }
    })
    .factory('AuthInterceptor', function ($q, $injector) {
        var LocalService = $injector.get('LocalService');

        return {
            request: function (config) {
                var token;
                if (LocalService.get('auth_token')) {
                    token = angular.fromJson(LocalService.get('auth_token')).token;
                }
                if (token) {
                    config.headers.Authorization = 'Bearer ' + token;
                }
                return config;
            },
            responseError: function (response) {
                if (response.status === 401 || response.status === 403) {
                    LocalService.unset('auth_token');
                    $injector.get('$state').go('anon.login');
                }
                return $q.reject(response);
            }
        }
    })
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    })
    .factory('CurrentUser', function(LocalService) {
        return {
            user: function() {
              console.log("in current user");
                if (LocalService.get('auth_token')) {
                  console.log("got auth token");
                    return angular.fromJson(LocalService.get('auth_token')).user;
                } else {
                    return {};
                }
            },
            car: function() {
              console.log("in current user");
                if (LocalService.get('auth_token')) {
                  console.log("got auth token");
                    return angular.fromJson(LocalService.get('auth_token')).car;
                } else {
                    return {};
                }
            }
        };
    })
    .factory('CarPosition', function ($http, Constants) {

        return {
            all: function () {
                return chats;
            },
            remove: function (chat) {
                chats.splice(chats.indexOf(chat), 1);
            },
            get: function (chatId) {
                for (var i = 0; i < chats.length; i++) {
                    if (chats[i].id === parseInt(chatId)) {
                        return chats[i];
                    }
                }
                return null;
            },
            save: function (position,carid) {
              var postion = $http.post(Constants.BaseURL + '/carposition/create?carId='+carid, position)
              .success(function (result) {
                postion = result;
                // console.log('success: '+JSON.stringify(result));
              });
              return postion;
            }
        };
    })
    // .factory('CurrentDriver', function(LocalService) {
    //     return {
    //         car: function() {
    //             if (LocalService.get('auth_token')) {
    //                 return angular.fromJson(LocalService.get('auth_token')).user;
    //             } else {
    //                 return {};
    //             }
    //         }
    //     };
    // })
    .factory('Chats', function () {

        return {
            all: function () {
                return chats;
            },
            remove: function (chat) {
                chats.splice(chats.indexOf(chat), 1);
            },
            get: function (chatId) {
                for (var i = 0; i < chats.length; i++) {
                    if (chats[i].id === parseInt(chatId)) {
                        return chats[i];
                    }
                }
                return null;
            }
        };
    })
    .factory('Blog', function (Constants, $http) {
        return {
            all: function () {
                var blogs = $http.get(Constants.BaseURL+'/blog').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return blogs;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (blogId) {
                var blog = $http.get(Constants.BaseURL+'/blog?id='+blogId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return blog;
            }
        }
    })
    .factory('Category', function (Constants, $http) {
        return {
            all: function () {
                var categories = $http.get(Constants.BaseURL+'/category').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);S
                    // err.status will contain the status code
                })
                return categories;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (categoryId) {
                var category = $http.get(Constants.BaseURL+'/category?id='+categoryId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return category;
            }
        }
    })
    .factory('Thread', function (Constants, $http) {
        return {
            all: function () {
                var threads = $http.get(Constants.BaseURL+'/thread').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return threads;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (threadId) {
                var thread = $http.get(Constants.BaseURL+'/thread?name='+threadId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return thread;
            },
            getByName: function (threadName) {
                var thread = $http.get(Constants.BaseURL+'/thread?name='+threadName).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return thread;
            }
        }
    })
    .factory('Video', function (Constants, $http) {
        return {
            all: function () {
                var videos = $http.get(Constants.BaseURL+'/video').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return videos;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (videoId) {
                var video = $http.get(Constants.BaseURL+'/video?id='+videoId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return video;
            }
        }
    })
    .factory('User', function (Constants, $http) {
        return {
            all: function () {
                var users = $http.get(Constants.BaseURL+'/user').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return users;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (userId) {
                var user = $http.get(Constants.BaseURL+'/user?id='+userId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return user;
            },
            getByName: function (userName) {
                var user = $http.get(Constants.BaseURL+'/user?id='+userName).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return user;
            }
        }
    })
    .factory('Profile', function (Constants, $http) {
        return {
            // all: function () {
            //     var updateUser = $http.get(Constants.BaseURL+'/updateProfile').success(function (resp) {
            //         console.log('Success', resp);
            //         // For JSON responses, resp.data contains the result
            //         //$scope.restaurants = resp.data;
            //     }, function (err) {
            //         console.error('ERR', err);
            //         // err.status will contain the status code
            //     })
            //     return updateUser;
            // },
            // //remove: function (chat) {
            // //    chats.splice(chats.indexOf(chat), 1);
            // //},
            update: function (profileId) {
                var profile = $http.get(Constants.BaseURL+'/user?id='+profileId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return profile;
            }
        }
    })
    .factory('Friend', function (Constants, $http) {
        return {
            all: function () {
                var friends = $http.get(Constants.BaseURL+'/friend').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return friends;
            },
            get: function (friendsId) {
                var friend = $http.get(Constants.BaseURL+'/friend?id='+friendsId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return friend;
            }
        }
    })
    // .factory('friendWith', function (Constants, $http) {
    //     return {
    //         all: function () {
    //             var friends = $http.get(Constants.BaseURL+'/friends').success(function (resp) {
    //                 console.log('Success', resp);
    //                 // For JSON responses, resp.data contains the result
    //                 //$scope.restaurants = resp.data;
    //             }, function (err) {
    //                 console.error('ERR', err);
    //                 // err.status will contain the status code
    //             })
    //             return friends;
    //         },
    //         //remove: function (chat) {
    //         //    chats.splice(chats.indexOf(chat), 1);
    //         //},
    //         get: function (friendById) {
    //             var friend = $http.get(Constants.BaseURL+'/friend?id='+friendId).success(function (resp) {
    //                 console.log('Success', resp);
    //                 // For JSON responses, resp.data contains the result
    //                 //$scope.restaurants = resp.data;
    //             }, function (err) {
    //                 console.error('ERR', err);
    //                 // err.status will contain the status code
    //             })
    //             return friend;
    //         }
    //     }
    // })

    // factory('FindUser', function (Constants, $http) {
    //     return {
    //         all: function () {
    //             var findUsers = $http.get(Constants.BaseURL+'/findUsers').success(function (resp) {
    //                 console.log('Success', resp);
    //                 // For JSON responses, resp.data contains the result
    //                 //$scope.restaurants = resp.data;
    //             }, function (err) {
    //                 console.error('ERR', err);
    //                 // err.status will contain the status code
    //             })
    //             return findUsers;
    //         },
    //         //remove: function (chat) {
    //         //    chats.splice(chats.indexOf(chat), 1);
    //         //},
    //         get: function (params) {
    //             var findUser = $http.get(Constants.BaseURL+'/find?'+params).success(function (resp) {
    //                 console.log('Success', resp);
    //                 // For JSON responses, resp.data contains the result
    //                 //$scope.restaurants = resp.data;
    //             }, function (err) {
    //                 console.error('ERR', err);
    //                 // err.status will contain the status code
    //             })
    //             return findUser;
    //         }
    //     }
    // })

    .factory('Image', function (Constants, $http) {
        return {
            // all: function () {
            //     var friends = $http.get(Constants.BaseURL+'/friends').success(function (resp) {
            //         console.log('Success', resp);
            //         // For JSON responses, resp.data contains the result
            //         //$scope.restaurants = resp.data;
            //     }, function (err) {
            //         console.error('ERR', err);
            //         // err.status will contain the status code
            //     })
            //     return friends;
            // },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            remove: function (imageId) {
                var profileImage = $http.get(Constants.BaseURL+'/profileImage?id='+imageId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return profileImage;
            }
        }
    })










    .service('XmppAuth', function(LocalService) {
            var Gab = {
                connection: null,
                start_time: null,
                log: function (msg) {
                  console.log(msg);
                },
                jid_to_id: function (jid) {
                    return Strophe.getBareJidFromJid(jid)
                        .replace(/@/g, "-")
                        .replace(/\./g, "-");
                },

                on_roster: function (iq) {
                    $(iq).find('item').each(function () {
                        var jid = $(this).attr('jid');
                        var name = $(this).attr('name') || jid;

                        // transform jid into an id
                        var jid_id = Gab.jid_to_id(jid);

                        console.log("on_roster",jid);

                        var contact = $("<li id='" + jid_id + "'>" +
                        "<div class='roster-contact offline'>" +
                        "<div class='roster-name'>" +
                        name +
                        "</div><div class='roster-jid'>" +
                        jid +
                        "</div></div></li>");

                        Gab.insert_contact(contact);
                    });

                    // set up presence handler and send initial presence
                    Gab.connection.addHandler(Gab.on_presence, null, "presence");
                    Gab.connection.send($pres());
                },

                pending_subscriber: null,

                on_presence: function (presence) { console.log(presence);
                    var ptype = $(presence).attr('type');
                    var from = $(presence).attr('from');
                    var jid_id = Gab.jid_to_id(from);

                      console.log("on_presence",from);

                    if (ptype === 'subscribe') {
                        // populate pending_subscriber, the approve-jid span, and
                        // open the dialog
                        Gab.pending_subscriber = from;
                        $('#approve-jid').text(Strophe.getBareJidFromJid(from));
                        $('#approve_dialog').dialog('open');
                    } else if (ptype !== 'error') {
                        var contact = $('#roster-area li#' + jid_id + ' .roster-contact')
                            .removeClass("online")
                            .removeClass("away")
                            .removeClass("offline");
                        if (ptype === 'unavailable') {
                            contact.addClass("offline");
                        } else {
                            var show = $(presence).find("show").text();
                            if (show === "" || show === "chat") {
                                contact.addClass("online");
                            } else {
                                contact.addClass("away");
                            }
                        }

                        var li = contact.parent();
                        li.remove();
                        Gab.insert_contact(li);
                    }

                    // reset addressing for user since their presence changed
                    var jid_id = Gab.jid_to_id(from);
                    $('#chat-' + jid_id).data('jid', Strophe.getBareJidFromJid(from));

                    return true;
                },

                on_roster_changed: function (iq) {
                    $(iq).find('item').each(function () {
                        var sub = $(this).attr('subscription');
                        var jid = $(this).attr('jid');
                        var name = $(this).attr('name') || jid;
                        var jid_id = Gab.jid_to_id(jid);

                        console.log("on_roster_changed",jid);

                        if (sub === 'remove') {
                            // contact is being removed
                            $('#' + jid_id).remove();
                        } else {
                            // contact is being added or modified
                            var contact_html = "<li id='" + jid_id + "'>" +
                                "<div class='" +
                                ($('#' + jid_id).attr('class') || "roster-contact offline") +
                                "'>" +
                                "<div class='roster-name'>" +
                                name +
                                "</div><div class='roster-jid'>" +
                                jid +
                                "</div></div></li>";

                            if ($('#' + jid_id).length > 0) {
                                $('#' + jid_id).replaceWith(contact_html);
                            } else {
                                Gab.insert_contact($(contact_html));
                            }
                        }
                    });

                    return true;
                },

                on_message: function (message) { console.log(message);
                    var full_jid = $(message).attr('from');
                    var jid = Strophe.getBareJidFromJid(full_jid);
                    var jid_id = Gab.jid_to_id(jid);

                    console.log("on_message",jid);

                    if ($('#chat-' + jid_id).length === 0) {
                        $('#chat-area').tabs('add', '#chat-' + jid_id, jid);
                        $('#chat-' + jid_id).append(
                            "<div class='chat-messages'></div>" +
                            "<input type='text' class='chat-input'>");
                    }

                    $('#chat-' + jid_id).data('jid', full_jid);

                    $('#chat-area').tabs('select', '#chat-' + jid_id);
                    $('#chat-' + jid_id + ' input').focus();

                    var composing = $(message).find('composing');
                    if (composing.length > 0) {
                        $('#chat-' + jid_id + ' .chat-messages').append(
                            "<div class='chat-event'>" +
                            Strophe.getNodeFromJid(jid) +
                            " is typing...</div>");

                        Gab.scroll_chat(jid_id);
                    }

                    var body = $(message).find("html > body");

                    if (body.length === 0) {
                        body = $(message).find('body');
                        if (body.length > 0) {
                            body = body.text()
                        } else {
                            body = null;
                        }
                    } else {
                        body = body.contents();

                        var span = $("<span></span>");
                        body.each(function () {
                            if (document.importNode) {
                                $(document.importNode(this, true)).appendTo(span);
                            } else {
                                // IE workaround
                                span.append(this.xml);
                            }
                        });

                        body = span;
                    }

                    if (body) {
                        // remove notifications since user is now active
                        $('#chat-' + jid_id + ' .chat-event').remove();

                        // add the new message
                        $('#chat-' + jid_id + ' .chat-messages').append(
                            "<div class='chat-message'>" +
                            "&lt;<span class='chat-name'>" +
                            Strophe.getNodeFromJid(jid) +
                            "</span>&gt;<span class='chat-text'>" +
                            "</span></div>");

                        $('#chat-' + jid_id + ' .chat-message:last .chat-text')
                            .append(body);

                        Gab.scroll_chat(jid_id);
                    }

                    return true;
                },

                scroll_chat: function (jid_id) {
                    var div = $('#chat-' + jid_id + ' .chat-messages').get(0);
                    div.scrollTop = div.scrollHeight;
                },

                presence_value: function (elem) {
                    if (elem.hasClass('online')) {
                        return 2;
                    } else if (elem.hasClass('away')) {
                        return 1;
                    }

                    return 0;
                },

                insert_contact: function (elem) { //console.log(elem);
                    var jid = elem.find('.roster-jid').text();
                    var pres = Gab.presence_value(elem.find('.roster-contact'));

                    var contacts = $('#roster-area li');

                    console.log("insert_contact",jid);

                    if (contacts.length > 0) {
                        var inserted = false;
                        contacts.each(function () {
                            var cmp_pres = Gab.presence_value(
                                $(this).find('.roster-contact'));
                            var cmp_jid = $(this).find('.roster-jid').text();

                            if (pres > cmp_pres) {
                                $(this).before(elem);
                                inserted = true;
                                return false;
                            } else if (pres === cmp_pres) {
                                if (jid < cmp_jid) {
                                    $(this).before(elem);
                                    inserted = true;
                                    return false;
                                }
                            }
                        });

                        if (!inserted) {
                            $('#roster-area ul').append(elem);
                        }
                    } else {
                        $('#roster-area ul').append(elem);
                    }
                },

                is_already_loggedin : function (){
                    var token;
                    if (LocalService.get('auth_chat_token')) {
                        token = angular.fromJson(LocalService.get('auth_chat_token'));
                        //console.log("yeah, he is already loggedin");
                        //console.log(token);
                        return token;
                    }
                    return false;
                },

                connect_me_now : function(userJid,userPass){  console.log(Strophe.Status);
                    var conn = new Strophe.Connection(
                        //'http://amartaxi.com:5280/http-bind');
                    'http://bosh.metajack.im:5280/xmpp-httpbind');

                    conn.connect(userJid, userPass, function (status) {
                            console.log(status);
                        if (status === Strophe.Status.CONNECTED) {
                            $(document).trigger('connected');
                        } else if (status === Strophe.Status.DISCONNECTED) {
                            $(document).trigger('disconnected');
                            console.log(status);
                        }
                    });

                    Gab.connection = conn;
                    //console.log(Gab.connection);
                    if(Gab.connection){
                        //getting necessary info from current session and set at local storage
                        LocalService.set('auth_chat_token', JSON.stringify(
                            {
                                'authcid':Gab.connection.authcid,
                                'authzid':Gab.connection.authzid,
                                'authenticated':Gab.connection.authenticated,
                                'jid':Gab.connection.jid,
                                'pass':Gab.connection.pass
                            }
                        ));

                        $('.chat-login-form').hide();
                        $('.chat-logout-form').show();
                    }
                },

                disconnect_me : function (){
                    Gab.connection.disconnect();
                    Gab.connection = null;
                    LocalService.unset('auth_chat_token');

                },
                send_ping: function (to) {
                  var ping = $iq({
                  to: to,
                  type: "get",
                  id: "ping1"}).c("ping", {xmlns: "urn:xmpp:ping"});

                  Gab.log("Sending ping to " + to + ".");
                  Gab.start_time = (new Date()).getTime();
                  Gab.connection.send(ping);
                },
                  handle_pong: function (iq) {
                    var elapsed = (new Date()).getTime() - Gab.start_time;
                    Gab.log("Received pong from server in " + elapsed + "ms");
                    Gab.connection.disconnect();
                    return false;
                  },
                  send_message: function(jid, body){
                    var message = $msg({to: jid,
                      "type": "chat"})
                    .c('body').t(body).up()
                    .c('active', {xmlns: "http://jabber.org/protocol/chatstates"});
                    Gab.connection.send(message);
                  },


                  chenge_presense: function(jid, body){
                    // var message = $msg({to: jid,
                    //   "type": "chat"})
                    // .c('body').t(body).up()
                    // .c('active', {xmlns: "http://jabber.org/protocol/chatstates"});
                    var press =  $iq({type: "get", id: "version1", to: "jabber.org"})
                        .c("query", {xmlns: "jabber:iq:version"});
                    // var press = $pres({to: "limon@amartaxi.com"});// $pres({type: "unavailable"});
                    var sss = Gab.connection.send(press);
                    console.log(sss);
                  },

            };
            //initially checking and if user refresh the page and the user already loggedin then it will automatically login with xmpp server
            var currentLoggedInUser = Gab.is_already_loggedin();
            if(currentLoggedInUser){
                Gab.connect_me_now(currentLoggedInUser.jid,currentLoggedInUser.pass);
            }

            //$(document).bind('connect', function (ev, data) {
            //  Gab.connect_me_now(data.jid,data.password);
            //});

            $(document).bind('connected', function () {
              console.log("limon connected");

                var iq = $iq({type: 'get'}).c('query', {xmlns: 'jabber:iq:roster'}); console.log(iq.toString());
                Gab.connection.sendIQ(iq, Gab.on_roster);
                Gab.connection.addHandler(Gab.on_roster_changed,"jabber:iq:roster", "iq", "set");
                Gab.connection.addHandler(Gab.on_message,null, "message", "chat");



            });

            $(document).bind('disconnected', function () {
              console.log("limon disconnected");

                console.log("gab disconnected");
                Gab.connection = null;
                Gab.pending_subscriber = null;

                $('#roster-area ul').empty();
                $('#chat-area ul').empty();
                $('#chat-area div').remove();

                $('#login_dialog').dialog('open');
            });

            $(document).bind('contact_added', function (ev, data) {
                console.log("contact_added");
                var iq = $iq({type: "set"}).c("query", {xmlns: "jabber:iq:roster"})
                    .c("item", data);
                Gab.connection.sendIQ(iq);

                var subscribe = $pres({to: data.jid, "type": "subscribe"});
                Gab.connection.send(subscribe);
            });

            return Gab;
        })
