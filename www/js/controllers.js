angular.module('starter.controllers', [])
    .controller('SelectCtrl', function ($scope, $timeout, $stateParams) {
        console.log("select control");

        ionic.material.ink.displayEffect();
    })
    .controller('AppCtrl', function ($scope, $ionicModal, $ionicPopover, $timeout) {
        // Form data for the login modal
        $scope.loginData = {};
        $scope.isExpanded = false;
        $scope.hasHeaderFabLeft = false;
        $scope.hasHeaderFabRight = false;

        var navIcons = document.getElementsByClassName('ion-navicon');
        for (var i = 0; i < navIcons.length; i++) {
            navIcons.addEventListener('click', function () {
                this.classList.toggle('active');
            });
        }

        ////////////////////////////////////////
        // Layout Methods
        ////////////////////////////////////////

        $scope.hideNavBar = function () {
            document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
        };

        $scope.showNavBar = function () {
            document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
        };

        $scope.noHeader = function () {
            var content = document.getElementsByTagName('ion-content');
            for (var i = 0; i < content.length; i++) {
                if (content[i].classList.contains('has-header')) {
                    content[i].classList.toggle('has-header');
                }
            }
        };

        $scope.setExpanded = function (bool) {
            $scope.isExpanded = bool;
        };

        $scope.setHeaderFab = function (location) {
            var hasHeaderFabLeft = false;
            var hasHeaderFabRight = false;

            switch (location) {
                case 'left':
                    hasHeaderFabLeft = true;
                    break;
                case 'right':
                    hasHeaderFabRight = true;
                    break;
            }

            $scope.hasHeaderFabLeft = hasHeaderFabLeft;
            $scope.hasHeaderFabRight = hasHeaderFabRight;
        };

        $scope.hasHeader = function () {
            var content = document.getElementsByTagName('ion-content');
            for (var i = 0; i < content.length; i++) {
                if (!content[i].classList.contains('has-header')) {
                    content[i].classList.toggle('has-header');
                }
            }

        };

        $scope.hideHeader = function () {
            $scope.hideNavBar();
            $scope.noHeader();
        };

        $scope.showHeader = function () {
            $scope.showNavBar();
            $scope.hasHeader();
        };

        $scope.clearFabs = function () {
            var fabs = document.getElementsByClassName('button-fab');
            if (fabs.length && fabs.length > 1) {
                fabs[0].remove();
            }
        };
    })

    .controller('LoginCtrl', function ($scope, $timeout, $stateParams) {
        $scope.$parent.clearFabs();
        $timeout(function () {
            $scope.$parent.hideHeader();
        }, 0);
        ionic.material.ink.displayEffect();
    })
    .controller('DriverLoginCtrl', function ($scope, $timeout, $stateParams) {
        $scope.$parent.clearFabs();
        $timeout(function () {
            $scope.$parent.hideHeader();
        }, 0);
        ionic.material.ink.displayEffect();
    })
    .controller('DriverLoginFormCtrl', function ($scope, $state, $timeout, $stateParams, CarAuth) {
        $scope.$parent.clearFabs();
        $timeout(function () {
            $scope.$parent.hideHeader();
        }, 0);
        ionic.material.ink.displayEffect();
        $scope.driver = {};
        //  $scope.driver.email = "asdas@email";
        //  $scope.driver.password="123123";
        $scope.driverLogin = function (cred) {
            console.log(cred);
            CarAuth.login(cred).success(function (result) {
                $state.go('app.driver_home');
            }).error(function (err) {
                // $scope.showToast('Login failed, Please Try Again', 'short','center');
            });
        }
    })
    .controller('DriverRegisterFormCtrl', function ($scope, $timeout, $state, $stateParams, CarAuth) {
        $scope.$parent.clearFabs();
        $timeout(function () {
            $scope.$parent.hideHeader();
        }, 0);
        ionic.material.ink.displayEffect();
        $scope.driver = {};
        //  $scope.driver.email = "asdas@email";
        //  $scope.driver.password="123123";
        $scope.driverRegister = function (cred) {
            console.log(cred);
            CarAuth.register(cred).then(function () {
                $state.go('app.driver_home');
                //  alert("reg");
            });

        }
    })
    .controller('PassengerLoginCtrl', function ($scope, $timeout, $stateParams, Auth) {
        $scope.$parent.clearFabs();
        $timeout(function () {
            $scope.$parent.hideHeader();
        }, 0);
        ionic.material.ink.displayEffect();

    })
    .controller('PassengerLoginFormCtrl', function ($scope, $timeout, $stateParams, Auth, $state) {
        $scope.$parent.clearFabs();
        $timeout(function () {
            $scope.$parent.hideHeader();
        }, 0);
        ionic.material.ink.displayEffect();
        $scope.passengerLogin = function (cred) {
            console.log(cred);
            Auth.login(cred).success(function (result) {
                $state.go('app.passenger_home');
            }).error(function (err) {
                // $scope.showToast('Login failed, Please Try Again', 'short','center');
            });
        }
    })
    .controller('PassengerRegisterFormCtrl', function ($scope, $timeout, $stateParams, Auth, $state) {
        $scope.$parent.clearFabs();
        $timeout(function () {
            $scope.$parent.hideHeader();
        }, 0);
        ionic.material.ink.displayEffect();
        $scope.passengerRegister = function (cred) {
            console.log(cred);
            Auth.register(cred).then(function () {
                $state.go('app.passenger_home');
                //  alert("reg");
            });

        }
    })
    .controller('FriendsCtrl', function ($scope, $stateParams, $timeout) {
        // Set Header
        $scope.$parent.showHeader();
        $scope.$parent.clearFabs();
        $scope.$parent.setHeaderFab('left');

        // Delay expansion
        $timeout(function () {
            $scope.isExpanded = true;
            $scope.$parent.setExpanded(true);
        }, 300);

        // Set Motion
        ionic.material.motion.fadeSlideInRight();

        // Set Ink
        ionic.material.ink.displayEffect();
    })

    .controller('ProfileCtrl', function ($scope, $stateParams, $timeout, $http, $interval, $cordovaGeolocation,$log) {
        // Set Header
        $scope.$parent.showHeader();
        $scope.$parent.clearFabs();
        $scope.isExpanded = false;
        $scope.$parent.setExpanded(false);
        $scope.$parent.setHeaderFab(false);

        // Set Motion
        $timeout(function () {
            ionic.material.motion.slideUp({
                selector: '.slide-up'
            });
        }, 300);

        $timeout(function () {
            ionic.material.motion.fadeSlideInRight({
                startVelocity: 3000
            });
        }, 700);

        // Set Ink
        ionic.material.ink.displayEffect();


        var scroll;
        $scope.hitCarPosition = function () {
            alert("start");
            scroll = $interval($scope.initHitCarPosition, 2000);

        };
        $scope.stopCarPosition = function () {
            alert("stop");
            $interval.cancel(scroll);
        };
        $scope.initHitCarPosition = function () {
            $http.get('http://localhost:1337/carposition').success(function (data) {
                console.log(data);
            });
        };
        $scope.latitude = 23.7867591;
        $scope.longitude = 90.4149649;

        $scope.map = { center: { latitude: $scope.latitude, longitude: $scope.longitude }, zoom: 18 };


        $scope.getMyPosition = function () {
            var posOptions = {timeout: 10000, enableHighAccuracy: false};
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {
                    var lat = position.coords.latitude;
                    var long = position.coords.longitude;
                    //alert("lat: " + lat + "long" + long);
                    $scope.latitude  = lat;
                    $scope.longitude = long;
                    $scope.marker.coords.latitude = $scope.latitude;
                    $scope.marker.coords.longitude = $scope.longitude;
                    $scope.map = { center: { latitude: $scope.latitude, longitude: $scope.longitude }, zoom: 18 };
                    console.log("lat: " + lat + " long: " + long)

                }, function (err) {
                    // error
                });
        };
        var posinterval;
        $scope.hitPosition = function () {
            alert("start");
            posinterval = $interval($scope.getMyPosition, 2000);

        };
        //$scope.getMyPosition();
        $scope.hitPosition();

        $scope.options = {scrollwheel: false};
        $scope.coordsUpdates = 0;
        $scope.dynamicMoveCtr = 0;
        $scope.marker = {
            id: 0,
            coords: {
                latitude: $scope.latitude,
                longitude: $scope.longitude
            },
            options: { draggable: false },
            events: {
                dragend: function (marker, eventName, args) {
                    $log.log('marker dragend');
                    var lat = marker.getPosition().lat();
                    var lon = marker.getPosition().lng();
                    $log.log(lat);
                    $log.log(lon);

                    $scope.marker.options = {
                        draggable: true,
                        labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
                        labelAnchor: "100 0",
                        labelClass: "marker-labels"
                    };
                }
            }
        };
        $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
            if (_.isEqual(newVal, oldVal))
                return;
            $scope.coordsUpdates++;
        });
        //$timeout(function () {
        //    $scope.marker.coords = {
        //        latitude: 42.1451,
        //        longitude: -100.6680
        //    };
        //    $scope.dynamicMoveCtr++;
        //    $timeout(function () {
        //        $scope.marker.coords = {
        //            latitude: 43.1451,
        //            longitude: -102.6680
        //        };
        //        $scope.dynamicMoveCtr++;
        //    }, 2000);
        //}, 1000);
    })

    .controller('DriverHomeCtrl', function ($scope, $stateParams, $timeout, $http, $interval, $cordovaGeolocation, CurrentUser, LocalService, $rootScope, CarPosition, XmppAuth) {
        // Set Header
        $scope.$parent.showHeader();
        $scope.$parent.clearFabs();
        $scope.isExpanded = false;
        $scope.$parent.setExpanded(false);
        $scope.$parent.setHeaderFab(false);

        // Set Motion
        $timeout(function () {
            ionic.material.motion.slideUp({
                selector: '.slide-up'
            });
        }, 300);

        $timeout(function () {
            ionic.material.motion.fadeSlideInRight({
                startVelocity: 3000
            });
        }, 700);
        $scope.isRunning = false;
        // Set Ink
        ionic.material.ink.displayEffect();
        var scroll;
        $scope.hitCarPosition = function () {
            $scope.isRunning = true;
            scroll = $interval($scope.getMyPosition, 1000);

        };
        $scope.stopCarPosition = function () {
            $scope.isRunning = false;
            $interval.cancel(scroll);
        };
        $scope.initHitCarPosition = function () {
            $http.get('http://localhost:1337/carposition').success(function (data) {
                console.log(data);
            });
        };
        $scope.getMyPosition = function () {
            var posOptions = {timeout: 10000, enableHighAccuracy: false};
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {
                    var lat = position.coords.latitude;
                    var long = position.coords.longitude;

                    $scope.car = CurrentUser.car().id;
                    $scope.pos = {};
                    $scope.pos.latitude = lat;
                    $scope.pos.longitude = long;

                    CarPosition.save($scope.pos, $scope.car);

                }, function (err) {
                    // error
                });
        };

        // $scope.xmppConnectMe = function()
        // {                  console.log("function in");
        //
        //
        //    $scope.conn = new Strophe.Connection(
        //       'http://amartaxi.com:5280/http-bind');
        //   //'http://bosh.metajack.im:5280/xmpp-httpbind');
        //   var userJid = "admin@amartaxi.com";
        //   var userPass = "admin";
        //   $scope.conn.connect(userJid, userPass, function (status) {
        //       if (status === Strophe.Status.CONNECTED) {
        //           // $(document).trigger('connected');
        //           console.log("connected");
        //       } else if (status === Strophe.Status.DISCONNECTED) {
        //           // $(document).trigger('disconnected');
        //           console.log("disconnected");
        //       }
        //   });
        //
        //   // Gab.connection = $scope.conn;
        //   console.log($scope.conn);
        //
        //                     console.log("function out");
        //
        // };
        // $scope.xmppDisconnectMe = function()
        // {
        //   $scope.conn.disconnect();
        // }
        $scope.xmppConnectMe = function () {

            //$(document).trigger('connect', {
            //  jid: $('#jid').val().toLowerCase(),
            //  password: $('#password').val()
            //});

            // var username = $('#jid').val().toLowerCase();
            // var password = $('#password').val();
             console.log($scope.user.message.jid);
             console.log($scope.user.message.jmessage);
            var username = $scope.user.message.jid;// "admin@amartaxi.com";
            var password = $scope.user.message.jmessage;//"admin";

            XmppAuth.connect_me_now(username,password);

            // $('#password').val('');
            //
            // $('.chat-login-form').hide();
            // $('.chat-logout-form').show();
        }

        $scope.xmppDisconnectMe = function(){
            XmppAuth.disconnect_me();
            // $('.chat-login-form').show();
            // $('.chat-logout-form').hide();
        }
        $scope.testMe = function(){
          var domain = Strophe.getDomainFromJid(XmppAuth.connection.jid);
                    console.log(domain);
                    XmppAuth.send_ping(domain);
                    // XmppAuth.handle_pong(domain);

        }
        $scope.handle_pong = function(){
          var domain = Strophe.getDomainFromJid(XmppAuth.connection.jid);
                    console.log(domain);
                    // XmppAuth.send_ping(domain);
                    XmppAuth.handle_pong(domain);

        }
        $scope.user = {};
        $scope.sendMessage = function(){
          // var domain = Strophe.getDomainFromJid(XmppAuth.connection.jid);
          // console.log(domain);
          // XmppAuth.send_ping(domain);

          // console.log($scope.user.message.jid);
          // console.log($scope.user.message.jmessage);

           XmppAuth.send_message("limon@amartaxi.com","first");

        }
        $scope.checkPresense = function(){
          // var domain = Strophe.getDomainFromJid(XmppAuth.connection.jid);
          // console.log(domain);
          // XmppAuth.send_ping(domain);

          // console.log("s");
          // console.log($scope.jmessage);
          //
           XmppAuth.chenge_presense("limon");

        }


    })
    .controller('PassengerHomeCtrl', function ($scope, $stateParams, $timeout, $http, $interval, $cordovaGeolocation, CurrentUser, LocalService, $rootScope, CarPosition) {
        // Set Header
        $scope.$parent.showHeader();
        $scope.$parent.clearFabs();
        $scope.isExpanded = false;
        $scope.$parent.setExpanded(false);
        $scope.$parent.setHeaderFab(false);

        // Set Motion
        $timeout(function () {
            ionic.material.motion.slideUp({
                selector: '.slide-up'
            });
        }, 300);

        $timeout(function () {
            ionic.material.motion.fadeSlideInRight({
                startVelocity: 3000
            });
        }, 700);

        // Set Ink
        ionic.material.ink.displayEffect();


        var scroll;
        $scope.hitCarPosition = function () {
            alert("start");
            scroll = $interval($scope.initHitCarPosition, 2000);

        };
        $scope.stopCarPosition = function () {
            alert("stop");
            $interval.cancel(scroll);
        };
        $scope.initHitCarPosition = function () {
            $http.get('http://localhost:1337/carposition').success(function (data) {
                console.log(data);
            });
        };
        $scope.latitude = 23.7867591;
        $scope.longitude = 90.4149649;

        $scope.map = { center: { latitude: $scope.latitude, longitude: $scope.longitude }, zoom: 18 };


        $scope.getMyPosition = function () {
            var posOptions = {timeout: 10000, enableHighAccuracy: false};
            $cordovaGeolocation
                .getCurrentPosition(posOptions)
                .then(function (position) {
                    var lat = position.coords.latitude;
                    var long = position.coords.longitude;
                    //alert("lat: " + lat + "long" + long);
                    $scope.latitude  = lat;
                    $scope.longitude = long;
                    $scope.marker.coords.latitude = $scope.latitude;
                    $scope.marker.coords.longitude = $scope.longitude;
                    $scope.map = { center: { latitude: $scope.latitude, longitude: $scope.longitude }, zoom: 18 };
                    console.log("lat: " + lat + " long: " + long)

                }, function (err) {
                    // error
                });
        };
        var posinterval;
        $scope.hitPosition = function () {
            alert("start");
            posinterval = $interval($scope.getMyPosition, 2000);

        };
        //$scope.getMyPosition();
        $scope.hitPosition();

        $scope.options = {scrollwheel: false};
        $scope.coordsUpdates = 0;
        $scope.dynamicMoveCtr = 0;
        $scope.marker = {
            id: 0,
            coords: {
                latitude: $scope.latitude,
                longitude: $scope.longitude
            },
            options: { draggable: false },
            events: {
                dragend: function (marker, eventName, args) {
                    $log.log('marker dragend');
                    var lat = marker.getPosition().lat();
                    var lon = marker.getPosition().lng();
                    $log.log(lat);
                    $log.log(lon);

                    $scope.marker.options = {
                        draggable: true,
                        labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
                        labelAnchor: "100 0",
                        labelClass: "marker-labels"
                    };
                }
            }
        };
        $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
            if (_.isEqual(newVal, oldVal))
                return;
            $scope.coordsUpdates++;
        });
        //$timeout(function () {
        //    $scope.marker.coords = {
        //        latitude: 42.1451,
        //        longitude: -100.6680
        //    };
        //    $scope.dynamicMoveCtr++;
        //    $timeout(function () {
        //        $scope.marker.coords = {
        //            latitude: 43.1451,
        //            longitude: -102.6680
        //        };
        //        $scope.dynamicMoveCtr++;
        //    }, 2000);
        //}, 1000);
        $scope.data = "Passenger Home";

    })
    .controller('ChoosePickupPointCtrl', function ($scope, $timeout, $stateParams, Auth, $state) {
        $scope.$parent.clearFabs();
        ionic.material.ink.displayEffect();
        $scope.data = "";
    })
    .controller('NearbyLocationsCtrl', function ($scope, $timeout, $stateParams, Auth, $state) {
        $scope.$parent.clearFabs();
        ionic.material.ink.displayEffect();
        $scope.data = "";
    })
    .controller('SelectOnMapCtrl', function ($scope, $timeout, $stateParams, Auth, $state) {
        $scope.$parent.clearFabs();
        ionic.material.ink.displayEffect();
        $scope.data = "";
    })
    .controller('PickupUpSelectedCtrl', function ($scope, $timeout, $stateParams, Auth, $state) {
        $scope.$parent.clearFabs();
        ionic.material.ink.displayEffect();
        $scope.data = "";
    })
    .controller('SelectDestinationCtrl', function ($scope, $timeout, $stateParams, Auth, $state) {
        $scope.$parent.clearFabs();
        ionic.material.ink.displayEffect();
        $scope.data = "";
    })
    .controller('SelectDestinationOnMapCtrl', function ($scope, $timeout, $stateParams, Auth, $state) {
        $scope.$parent.clearFabs();
        ionic.material.ink.displayEffect();
        $scope.data = "";
    })
    .controller('ItinerarySelectedCtrl', function ($scope, $timeout, $stateParams, Auth, $state) {
        $scope.$parent.clearFabs();
        ionic.material.ink.displayEffect();
        $scope.data = "";
    })
    .controller('ActivityCtrl', function ($scope, $stateParams, $timeout) {
        $scope.$parent.showHeader();
        $scope.$parent.clearFabs();
        $scope.isExpanded = true;
        $scope.$parent.setExpanded(true);
        $scope.$parent.setHeaderFab('right');

        $timeout(function () {
            ionic.material.motion.fadeSlideIn({
                selector: '.animate-fade-slide-in .item'
            });
        }, 200);

        // Activate ink for controller
        ionic.material.ink.displayEffect();
    })

    .controller('GalleryCtrl', function ($scope, $stateParams, $timeout) {
        $scope.$parent.showHeader();
        $scope.$parent.clearFabs();
        $scope.isExpanded = true;
        $scope.$parent.setExpanded(true);
        $scope.$parent.setHeaderFab(false);

        // Activate ink for controller
        ionic.material.ink.displayEffect();

        ionic.material.motion.pushDown({
            selector: '.push-down'
        });
        ionic.material.motion.fadeSlideInRight({
            selector: '.animate-fade-slide-in .item'
        });

    })

;
